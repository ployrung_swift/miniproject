//
//  SwiftUIView.swift
//  miniProject_61160154
//
//  Created by informatics on 3/5/21.
//
import Foundation
import SwiftUI
import Combine

struct Task : Identifiable{
    var id = String()
    var toDoItem = String()
    
}
class TaskStore : ObservableObject{
    @Published var tasks = [Task]()
}
