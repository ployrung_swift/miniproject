//
//  ContentView.swift
//  miniProject_61160154
//
//  Created by informatics on 3/5/21.
//

import SwiftUI
import Combine

struct ContentView: View {
    @ObservedObject var taskStore = TaskStore()
    @State var newToDo : String = ""
    @State var list = String()
    @State var txtField = "Enter in a new task"
    
    var searchBar : some View {
        HStack{
            
            TextField("\(txtField)"
                      , text:self.$newToDo)
                .padding()
                
            Button(action: {addNewToDo()}, label: {
                Text("Add New")
                    .font(.body)
                    .padding()
                                        .foregroundColor(.white)
                    .background(Color.blue)
                    .cornerRadius(50)

                    
            })

        }
    }
    func addNewToDo() {
        taskStore.tasks.append(Task(id: String(taskStore.tasks.count + 1), toDoItem: newToDo))
    }
    
    var body: some View {
        NavigationView {
            VStack{
                searchBar.padding()
                Text("M Y    L  I  S  T")
                    .padding()
                    .font(/*@START_MENU_TOKEN@*/.largeTitle/*@END_MENU_TOKEN@*/)
                    .foregroundColor(.white)
                    .background(Color.orange)
                    .cornerRadius(10)
                List{
                    ForEach (self.taskStore.tasks) {
                        task in
                        Text(task.toDoItem)
                            .foregroundColor(.blue)
                    }
                    .onMove(perform: self.move)
                    .onDelete(perform: self.delete)
                }
                .navigationBarTitle("To-Do List")
                .padding()
                .navigationBarItems(trailing: EditButton())
                .background(Color.orange)
                
            }

            
        }
    }
    func move(from source : IndexSet, to destination : Int) {
        taskStore.tasks.move(fromOffsets: source, toOffset: destination)
        
    }
    func delete(at offsets: IndexSet){
        taskStore.tasks.remove(atOffsets: offsets)
    }
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
        
    }
}
