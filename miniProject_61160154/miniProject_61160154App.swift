//
//  miniProject_61160154App.swift
//  miniProject_61160154
//
//  Created by informatics on 3/5/21.
//

import SwiftUI

@main
struct miniProject_61160154App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
